*********************************************************************
                     SOBREVIVIENTE v1.0
           Copyright 2002 Miguel Angel Pineda Nieto

            http://www.geocities.com/miguel_pineda
                    miguel_pineda@yahoo.com

*********************************************************************

Disclaimer:
This program comes without any warranties, so use it at your own 
risk! I don't take responsability for any damages this program 
(may) produce.

This program is Freeware. 

You are NOT allowed to sell it or change it without permission.


*********************************************************************
Contents:
*********************************************************************

- The Story So Far...
- Overview
- Controls
- Special Items
- Credits


*********************************************************************
         THE STORY SO FAR...
*********************************************************************

You were once a normal human being. That is, before the "abduction".
Men of an unknown agency have been capturing men and women from 
around the globe for no apparent reason. This time it was your turn.

You've been locked up, stripped of your identity and been subject to
many radical and painful experiments, and as a result, your body is 
now a mixture of plastic, metal and flesh. You've had to endure this
for more than half a year now, and slowly, you've become less a man
and more a machine.

Today things are slightly different. They've developed a new kind of
weapon and are conducting tests. You're given the choice to help 
test this weapon or keep being a lab rat. Without hesitation you 
take the weapon and ask "Ok. What do I do now?" They respond:

"Simple. Survive and you will get to see another day..."



...The mutants are increasing in numbers. They're faster and their
shots are more accurate. It won't be too long before they're too 
much for you.

You can't wait.


*********************************************************************
         OVERVIEW
*********************************************************************


Sobreviviente is a fast 3D arcade-style shooter. The object of the 
game is to eliminate the green mutants before the time reaches zero
while racking up as many points as you can.


Minimum requirements:

- 350 mhz CPU
- 8mb 100% OpenGL compliant videocard
- 32 Mb RAM

The game runs in 640x480 16 bit color mode. At the moment it can't
be changed


*********************************************************************
         CONTROLS
*********************************************************************


                W - Run forward
                S - Run backwards
                D - Sidestep right
                A - Sidestep left
 Mouse left-right - Direction
Left mouse button - Shoot
              ESC - Exit game

You can control the camera's height moving the mouse up and down to
give you a better perspective of the field.


*********************************************************************
        SPECIAL ITEMS 
*********************************************************************

An item box will appear on one of the field's four corners right 
away when the round begins and later, when the timer reaches 50.

The item box may contain any of the following:

- 1500 extra points

- 25 extra health (health is shown on the left side of the screen)

- 100 Machine gun rounds (machine gun ammunition is shown on the right
  side of the screen)


*********************************************************************
        CREDITS
*********************************************************************

Program, artwork and music by Miguel Angel Pineda Nieto
This game uses:

- Allegro by Shawn Hargraves
  http://alleg.sf.net and http://www.allegro.cc

- Allegro GL by George Foot  
  http://allegrogl.sf.net

- FMOD
  http://www.fmod.com

- Escultor 2.0 
  http://www.geocities.com/miguel_pineda
